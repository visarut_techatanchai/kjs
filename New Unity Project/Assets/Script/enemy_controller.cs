﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class enemy_controller : MonoBehaviour
{
    public float speed = 10f;
    public int damage = 1;
    private Transform target;
    private int wavepointIndex = 0;
    NavMeshAgent agent;
    public Transform spawnPoint1;
    public Transform spawnPoint2;
    public Transform spawnPoint3;
    public Transform[] spawnList;
    public Transform[] waypointArray;

    private void Start()
    {
        spawnList = new Transform[3];

        spawnList[0] = spawnPoint1;
        spawnList[1] = spawnPoint2;
        spawnList[2] = spawnPoint3;

        GameObject spawnPoint = FindClosestEnemy();
        Debug.Log(spawnPoint.name == spawnPoint1.name);
        Debug.Log(spawnPoint.name == spawnPoint2.name);
        Debug.Log(spawnPoint.name == spawnPoint3.name);

        if  (spawnPoint.name == spawnPoint1.name)
        {
            Debug.Log("SPAWNING POINT1");
            waypointArray = Waypoints1.waypoint;
        }

        else if (spawnPoint.name == spawnPoint2.name)
        {
            Debug.Log("SPAWNING POINT2");
            waypointArray = Waypoints2.waypoint;
        }

        else if (spawnPoint.name == spawnPoint3.name)
        {
            Debug.Log("SPAWNING POINT3");
            waypointArray = Waypoints3.waypoint;
        }
        agent = GetComponent<NavMeshAgent>();
        target = waypointArray[0];
    }

    private void Update()
    {
        agent.SetDestination(target.position);

        if (Vector3.Distance(transform.position, target.position) <= 1f)
        {
            GetNextWayPoint();
        }
    }

    void GetNextWayPoint()
    {
        if (wavepointIndex >= waypointArray.Length - 1)
        {
            GameManager.health -= damage;
            Destroy(gameObject);
            return;
        }
        wavepointIndex++;
        target = waypointArray[wavepointIndex];
    }

    public GameObject FindClosestEnemy()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("SpawningPoints");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        Debug.Log(closest);
        return closest;
    }
}

