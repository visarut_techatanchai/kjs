﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject enemy_1_object = null;
    public Transform spawnPoint1;
    public Transform spawnPoint2;
    public Transform spawnPoint3;

    public float timeBetweenWaves = 5;
    private float countdownTime = 2f;
    private int waveNumber = 20;

    public static int maxHealth = 20;
    public static int health;
    public HealthBar healthBar;

    [SerializeField]
    private GameObject gameOverUI;

    private void Start()
    {
        health = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        healthBar.SetHealth(health);
    }
    void Update()
    {
        healthBar.SetHealth(health);

        if (health < 1)
        {
            gameOverUI.SetActive(true);
        }

        if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 0 && health > 0)
        {
            if (countdownTime <= 0f)
            {
                StartCoroutine(SpawnWave());
                countdownTime = timeBetweenWaves;
            }

            countdownTime -= Time.deltaTime;
        }
    }
    IEnumerator SpawnWave()
    {
        Debug.Log("spawnWave");
        for (int i = 0; i < waveNumber; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(0.5f);
        }
        waveNumber++;

    }

    void SpawnEnemy()
    {
        int spawning_point = Random.Range(1, 4);
        Debug.Log(spawning_point);

        if(Mathf.Round(spawning_point) == 1)
        {
            Debug.Log("ENEMYYYYYY");
            GameObject enemy = Instantiate(enemy_1_object, spawnPoint1.position, spawnPoint1.rotation);
            
        }
        else if(Mathf.Round(spawning_point) == 2)
        {
            Debug.Log("ENEMYYYYYY");
            GameObject enemy = Instantiate(enemy_1_object, spawnPoint2.position, spawnPoint2.rotation);
        }
        else if (Mathf.Round(spawning_point) == 3)
        {
            Debug.Log("ENEMYYYYYY");
            GameObject enemy = Instantiate(enemy_1_object, spawnPoint3.position, spawnPoint3.rotation);
        }
    }

}
